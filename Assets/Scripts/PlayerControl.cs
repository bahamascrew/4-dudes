﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    public PlayerShooting shootingScript;
    public PlayerMoving movingScript;

    void Awake()
    {
        InControl(false);
    }

    public void InControl(bool active)
    {
        shootingScript.enabled = active;
        movingScript.enabled = active;
    }
}
