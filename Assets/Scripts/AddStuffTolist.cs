﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AddStuffTolist : MonoBehaviour
{
    public string[] tagsToCheckFor;

    public bool checkingFor3DObjects = true;

    private List<GameObject> gameObjects = new List<GameObject>();

    void Awake()
    {
        //Sanity checking if the tags actually exist
        
    }

    
    public List<GameObject> GetListOfObject()
    {
        return gameObjects;
    }

    private bool checkIf3D()
    {
        return checkingFor3DObjects;
    }
    //____________________________3D___________________

    void OnTriggerEnter(Collider other)
    {
        if (checkIf3D())
        {
            CheckIfItemShouldBeAdded(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (checkIf3D())
        {
            CheckIfItemShouldBeRemoved(other.gameObject);
        }
    }

    //______________________________2D_____________

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!checkIf3D())
        {
            CheckIfItemShouldBeAdded(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (!checkIf3D())
        {
            CheckIfItemShouldBeRemoved(other.gameObject);
        }
    }

    void CheckIfItemShouldBeAdded(GameObject theObject)
    {
        for (int i = 0; i < tagsToCheckFor.Length; i++)
        {
            if (theObject.CompareTag(tagsToCheckFor[i]))
            {
                AddObjectTolist(theObject);
            }
        }
    }

    void CheckIfItemShouldBeRemoved(GameObject theObject)
    {
        for (int i = 0; i < gameObjects.Count; i++)
        {
            if (gameObjects[i] == theObject)
            {
                //Remove object from list
                RemoveObjectFromList(i);
            }
        }
    }

    /// <summary>
    /// Adds the GameObject to the list
    /// </summary>
    /// <param name="theObject"></param>
    void AddObjectTolist(GameObject theObject)
    {
        gameObjects.Add(theObject);
    }

    /// <summary>
    /// Removes the GameObject at the index specified
    /// </summary>
    /// <param name="index"></param>
    void RemoveObjectFromList(int index)
    {
        gameObjects.RemoveAt(index);
    }
}