﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{

    public float maxHealth = 100;
    private float currentHealth;

    private bool destroyed;

    public void Damage(float damage)
    {
        currentHealth -= damage;
        if(currentHealth <= 0 )
        {
            if(!destroyed)
            {
                destroyed = true;
                Destroy(gameObject);
            }
        }
    }
}
