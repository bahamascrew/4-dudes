﻿using UnityEngine;
using System.Collections;

public class ControllingCharacters : MonoBehaviour
{

    private PlayerControl[] players;
    private int index;
	
	// Update is called once per frame
	void Update () 
    {
	    for (int i = 0; i < players.Length; i++)
	    {
	        if (Input.GetKeyDown( (i + 1).ToString()))
	        {
	            SetCharToBeInControl(i);
	        }
	    }
	}

    public void InsertPlayer( PlayerControl character)
    {
        players[index] = character;
        index++;
    }

    public void initializeArray(int amount)
    {
        players = new PlayerControl[amount];
    }

    public void InitializeControl()
    {
        //SetCharToBeInControl(0);
    }

    void SetCharToBeInControl(int playerIndex)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (i == playerIndex)
            {
                players[i].InControl(true);
                
            }
            else
            {
                players[i].InControl(false);
            }
                

        }
    }
}
