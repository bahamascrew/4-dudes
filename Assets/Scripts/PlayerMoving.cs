﻿using UnityEngine;
using System.Collections;

public class PlayerMoving : MonoBehaviour
{

    private Rigidbody2D rbody2D;
    public float speed;


    void Awake()
    {
        rbody2D = GetComponent<Rigidbody2D>();
    }
	// Update is called once per frame
	void Update ()
	{
	    float horizontal = Input.GetAxisRaw("Horizontal");
	    float vertical = Input.GetAxisRaw("Vertical");

        rbody2D.velocity = new Vector2(horizontal,vertical)*speed;
	}
}
