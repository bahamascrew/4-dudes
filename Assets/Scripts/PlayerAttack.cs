﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

    public float timeBetweenAttacks = 2;
    public float damage = 2;

    private float nextTimeToAttack;

    public AddStuffTolist objectsInTrigger;

	// Update is called once per frame
	void Update () 
    {
	    if(nextTimeToAttack < Time.time)
        {
            Attack();
            nextTimeToAttack = Time.time + timeBetweenAttacks;
        }
	}

    void Attack()
    {
        var enemies = objectsInTrigger.GetListOfObject();
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] == null)
            {
                enemies.RemoveAt(i);
                continue;;
            }
                enemies[i].GetComponent<Health>().Damage(damage);
        }
    }
}
