﻿using System.CodeDom.Compiler;
using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    [System.Serializable]
    public class Positions
    {
        public bool useTransform = false;

        //The center position of the spawning
        public Vector2 position;
        public Transform theTransform;

        //range of where we are moving
        public Vector2 lengthFromPosition;
    }

    private int randomPositionIndex = 0;
    public bool randomPosition;

    

    public Positions[] spawnPositions;


    [System.Serializable]
    public class Wave
    {
        public float randomXLength = 1;
        public float randomDelayBetweenSpawn = 0;
        public int amountOfEnemies = 0;
        public GameObject[] enemies;
        public float waitTillNextWaveTime = 5;
    }

    public int resetWave = 0;

    public Wave[] wave;

    private float nextSpawnTime = 0;
    private int currentWave = -1; //set to -1 because when i call to start a wave i do currentWave++
    private bool spawnEnemies = true;
    private int enemiesSpawned = 0;

    private float nextWaveStartTime = 0;

    

    void Start()
    {
        //DEBUG CODE DELETE! TODO DELETE ME!
        currentWave++;
    }



    void Cake()
    {
        if (nextSpawnTime < Time.time)
        {
            if (currentWave < wave.Length)
            {
                if (nextWaveStartTime < Time.time)
                {
                    if (wave[currentWave].amountOfEnemies > enemiesSpawned)
                    {
                        enemiesSpawned++;

                        nextSpawnTime = Time.time + Random.Range(0f, wave[currentWave].randomDelayBetweenSpawn);

                        if (randomPosition)
                        {
                            randomPositionIndex = Random.Range(0, spawnPositions.Length);
                        }
                        else
                        {
                            randomPositionIndex++;
                            if (randomPositionIndex >= spawnPositions.Length)
                            {
                                randomPositionIndex = 0;
                            }
                        }

                        Vector2 tempPosition = new Vector2(0,0);
                        if (spawnPositions[randomPositionIndex].useTransform)
                        {
                            tempPosition = spawnPositions[randomPositionIndex].theTransform.position;
                        }
                        else
                        {
                            tempPosition = spawnPositions[randomPositionIndex].position;
                        }

                        //set the position where this object should spawn
                        tempPosition += new Vector2(Random.Range(-spawnPositions[randomPositionIndex].lengthFromPosition.x,spawnPositions[randomPositionIndex].lengthFromPosition.x),
                            Random.Range(-spawnPositions[randomPositionIndex].lengthFromPosition.y, spawnPositions[randomPositionIndex].lengthFromPosition.y));

                        //Instantiate();
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnEnemies && nextSpawnTime < Time.time)
        {
            if (currentWave < wave.Length)
            {
                if (nextWaveStartTime < Time.time)
                {
                    if (wave[currentWave].amountOfEnemies > enemiesSpawned)
                    {
                        enemiesSpawned++;

                        //Set the next spawn time
                        nextSpawnTime = Time.time + Random.Range(0f, wave[currentWave].randomDelayBetweenSpawn);

                        //Set spawn position
                        var temp = transform.position;
                        temp.x += Random.Range(-wave[currentWave].randomXLength, wave[currentWave].randomXLength);

                        //Create the enemy
                        Instantiate(wave[currentWave].enemies[Random.Range(0, wave[currentWave].enemies.Length)],
                            temp, Quaternion.identity);
                        Debug.Log("Spawned an enemy");

                        //GameManager.instance.SpawnedAnEnemy();
                    }
                    else
                    {
                        WaveDone();
                    }
                }
            }
        }
        if (!spawnEnemies)
        {
            /*if (GameManager.instance.spawnNextWave)
            {

                currentWave++;
                spawnEnemies = true;
                if (currentWave >= wave.Length)
                {
                    GameManager.instance.PlayCutScene();
                }
            }*/
        }

    }

    private void WaveDone()
    {
        nextWaveStartTime = wave[currentWave].waitTillNextWaveTime + Time.time;
        enemiesSpawned = 0;

        spawnEnemies = false;
    }
}
