﻿using System.Collections;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class EnemyAI : MonoBehaviour
{

    public Transform target;

    //update rate for getting a path
    public float updatePerSecond = 2f;

    private Seeker seeker;
    private Rigidbody2D rb;

    //the calculated path
    public Path path;

    //The AI's speed per second
    public float speed = 300f;
    public ForceMode2D fMode;

    [HideInInspector]
    public bool pathIsEnded = false;

    //The max distance from the AI to a waypoint
    public float nextWayPointDistance = 3;

    //The next WayPoint
    private int currentWayPoint;


	// Use this for initialization
	void Start ()
	{

	    seeker = GetComponent<Seeker>();
	    rb = GetComponent<Rigidbody2D>();

	    if (target == null)
	    {
	        Debug.LogError("NO TARGET");
            return;
	    }

	    StartCoroutine(UpdatePath());
	}

    void FixedUpdate()
    {
        if (target == null)
        {
            //TODO inser a player search here
            return;
        }

        //TODO always look towards direction you are going?
        if (path == null)
        {
            return;
        }
        if (currentWayPoint >= path.vectorPath.Count)
        {
            if(pathIsEnded)
                return;

            Debug.Log("End of path reached");
            pathIsEnded = true;
            return;
        }
        pathIsEnded = false;

        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWayPoint] - transform.position).normalized;
        dir *= speed*Time.fixedDeltaTime;
        
        //Move the AI
        rb.AddForce(dir, fMode);

        if (Vector3.Distance(transform.position, path.vectorPath[currentWayPoint]) < nextWayPointDistance)
        {
            currentWayPoint++;
            return;
        }
    }

    public void OnPathComplete(Path p)
    {
        Debug.Log("We got a path. error? " + p.error);
        if (!p.error)
        {
            path = p;
            currentWayPoint = 0;
        }
    }

    IEnumerator UpdatePath()
    {
        if (target == null)
        {
            //TODO inser a player search here
            yield break;
        }

        //start a new path to the target position, return the result to the OnPathComplete method
        seeker.StartPath(transform.position, target.position, OnPathComplete);

        yield return new WaitForSeconds(updatePerSecond);
        StartCoroutine(UpdatePath());
    }
}
