﻿using UnityEngine;
using System.Collections;

public class CreateChars : MonoBehaviour
{
    public ControllingCharacters cc;
    public GameObject character;
    public int characterCreateAmount = 4;

    private int index = 0;

	// Use this for initialization
	void Start () 
    {
        cc.initializeArray(characterCreateAmount);
	    for (int i = 0; i < characterCreateAmount; i++)
	    {
            CreateCharacter();
	    }
        cc.InitializeControl();
	}


    void CreateCharacter()
    {
        GameObject clone = (GameObject)Instantiate(character, new Vector3(1, 1, 1), Quaternion.identity);


        clone.transform.GetChild(0).RotateAround(clone.transform.position, Vector3.forward, 90*index);
        index++;

        cc.InsertPlayer(clone.GetComponent<PlayerControl>());
    }
}
